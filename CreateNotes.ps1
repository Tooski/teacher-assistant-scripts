<# 
    Extracts notes to student's folders.
    Created by: Josef Nosov
#>

$filePath = "Z:\Downloads\So14-TCSS342-HW1 Submission-32306";




Import-Csv ($filePath  +"\output3.csv") | Foreach-Object{
$notes = @()
    
    if($_."Followed Instructions" -ne 10) {
        $notes += "* Did not follow instructions. -" + $_."Followed Instructions"
    }

    if($_."Redundancy" -ne 15) {
        $notes += "* Too much redundant code or inappropriate codestyle. -"
    }

   
    if($_."Incorrect Output" -eq "TRUE") {
        $notes += "* Ballots are listed, but nothing else. -50"
    } else {
       if($_."No Compile Good Code Length" -ne 0) {
        $notes += "* Cannot compile. -" + (75 - $_."No Compile Good Code Length")

       } else {


    if($_."Output" -ne 75) {
        if($_."Compiled" -ne 25) {
            $notes += "* Did not compile successfully. -" + $_."Compiled" 
        }

        $flaws = $_."Flaws";
        if($flaws -gt 0 -and $flaws -le 5) {
            $notes += "* Output contains minor flaws. -" + $flaws 
        } elseif ($flaws -gt 5 -and $flaws -le 25) {
            $notes += "* Output contains major flaws. -" + $flaws
        }
        }
    }
    }


       if($_."Data Structure Used Properly" -ne 0) {
            $notes += "* Did not use appropriate data structures. -" + $_."Data Structure Used Properly"
       }
   
   if($_."Main Method Present" -ne 0) {
     $notes += "* Provided main method has been modified. -" + $_."Main Method Present"
   }
  
    if($_."More than 2 Loops" -ne 0) {
        $notes += "More than two loops within methods. -" + $_."More than 2 Loops"

    }
    if($_."Late" -eq "TRUE") {
        $notes += "Assignment has been turned in late. -10%"
    }

 

    if($notes.Count -eq 0) {
      $notes += "* No issues found, great job!"
      }

      if($_."Other Comments" -ne 0) {
      $notes += ("* Other Comments: " + $_."Other Comments")
      }

     $notes += "================================="
     $notes += "Score : " + $_.Result + " / 100"
    $notes | Out-File ($filePath + "\Students\" + $_.Name + "\notes.txt")
    

}

