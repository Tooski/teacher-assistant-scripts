﻿<# 
    Creates path if does not exists and unzips the files to the particular locations 
    Created by: Josef Nosov
#>

$filePath = "Z:\Downloads\So14-TCSS342-HW1 Submission-32306";

# Creates an array of student names.
$studentNames = @();   			

$shell= New-Object -com shell.application

# Parses the name of the zip files, creates folder and extracts zip into the folder.
Get-ChildItem $filePath | ForEach-Object{ 

    if($_.Extension -eq ".zip") {
        $name = $_.Name.Substring(0, $_.Name.IndexOf("_"))
        $studentNames += $name
        $studentName =  $_.DirectoryName + "\Students\" + $name
        if(!(Test-Path -Path $studentName )){
            New-Item -ItemType directory -Path $studentName
            $shell.namespace($studentName).Copyhere($shell.namespace($_.fullName).items())
        } else {
            Write-Error "Path already exists!"
        }
    }
}





$outputFile = '.\output.csv'

# Creates a csv file.
if(!(Test-Path -Path ($filePath + "\" + $outputFile))){
    $column = "Name","Followed Instructions","Redundancy","Output",
    "Compiled","Flaws","Failed Test","No Compile Good Code Length",
    "Incorrect Output","Data Structure Used Properly","Main Method Present",
    "More than 2 Loops","Late","Result", "Other Comments"
    $p = 2;
    $studentNames | %{
    $user = new-object psobject
    for($i = 0; $i -lt $column.Count; $i++) {
        if($column[$i] -eq "Name") {
           $value = $_
        } elseif ($column[$i] -eq "Result") {
            $value = "=((B" + $p + "+C"+  $p +"+D"+ $p +"-J"+$p + "-K" +$p +"-L" +$p + ")*IF(M" + $p +",0.9,1))"
        } elseif ($column[$i] -eq "Late" -or $column[$i] -eq "Incorrect Output") {
            $value = "FALSE"
        } elseif ($column[$i] -eq "Followed Instructions") {
            $value = 10
        } elseif ($column[$i] -eq "Redundancy") {
            $value = 15
        } elseif ($column[$i] -eq "Compiled") {
            $value = 25
        } elseif ($column[$i] -eq "Output") {
            $value = "=IF(I"+$p+"=FALSE,IF(H"+$p+"=0,E"+$p+"+25-F"+$p+"+25-G"+$p+",H"+$p+"), 25)"
        } else {
           $value = 0
        }
        $user | add-member -membertype noteproperty -name $column[$i] -value $value
    }
    $user
    $p++;
    } | export-csv ($filePath + "\" + $outputFile) -notype
} else {
    Write-Error "CSV already exists!"
}